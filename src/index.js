import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import './index.scss';
// import App from './App';
import Layout from './Layout';
import { BrowserRouter as Router } from "react-router-dom"
import reportWebVitals from './reportWebVitals';
import Header from './Components/Common/Header';
import Footer from './Components/Common/Footer';

ReactDOM.render(
  //  <React.StrictMode>
  //    <App /> 
  //  </React.StrictMode>,

  <Router>
    <Header />
    <div className="content">
      <Layout />
    </div>
    <Footer />
  </Router>,

  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
