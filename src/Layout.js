import React from "react";
import { Route, Switch } from "react-router-dom";
import About from "./Components/About";
import Contact from "./Components/Contact";
import Home from "./Components/Home";
import Login from "./Components/form/Login";
import PersonalInfo from "./Components/enquiry/PersonalInfo";
import MotorEnquiryPage from "./Components/enquiry/MotorEnquiry";
import MotorQuote from "./Components/quote/MotorQuote";
import ProposalForm from "./Components/form/ProposalForm";
import SummaryPage from "./Components/form/SummaryPage";


const Layout = () => {
    return (
        <Switch>
            <Route exact path='/' component={Home} />,
            <Route path='/login' component={Login} />,
            <Route path='/About' component={About} />,
            <Route path="/contact" component={Contact} />
            <Route path="/personal-info" component={PersonalInfo}/>
            <Route path="/motor-enquiry-page" component={MotorEnquiryPage} />
            <Route path="/motor-quote" component={MotorQuote} />
            <Route path="/proposal-form" component={ProposalForm} />
            <Route path="/summary" component={SummaryPage} />
        </Switch>
    )
};

export default Layout;