import React from "react";
import Navbar from "./Components/Navbar";
import HeroImage from "./Components/HeroImage";
import Section from "./Components/Section";
import PolicyList from "./Components/PolicyList";
import MotorSummary from "./Components/MotorSummary";
import MotorProposal from "./Components/MotorProposal";
import Try from "./Components/Try";
import EnquiryPage from "./Components/EnquiryPage";


function App() {
	return (
		<div>
			{/* <Navbar/>
			<HeroImage/>
			<Section/> */}
			{/* <MotorSummary/> */}
			{/* <PolicyList/> */}

			{/* this is for proposal and summary  */}
			<MotorProposal />
			<div>
				{/* <Try/> */}
			</div>
			<div>
				{/* <EnquiryPage/> */}

			</div>
		</div>
	);
}
export default App;