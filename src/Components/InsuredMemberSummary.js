import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFemale, faMale } from '@fortawesome/free-solid-svg-icons'

function InsuredMemberSummary() {
    return (
        <div>
            <ul className="nav nav-tabs" id="myTab" role="tablist">
                <li className="nav-item" role="presentation">
                    <button className="nav-link active" id="you-tab" data-bs-toggle="tab" data-bs-target="#you" type="button" role="tab" aria-controls="you" aria-selected="true">You</button>
                </li>
                <li className="nav-item" role="presentation">
                    <button className="nav-link" id="spouse-tab" data-bs-toggle="tab" data-bs-target="#spouse" type="button" role="tab" aria-controls="spouse" aria-selected="false">Spouse</button>
                </li>
                <li className="nav-item" role="presentation">
                    <button className="nav-link" id="father-tab" data-bs-toggle="tab" data-bs-target="#father" type="button" role="tab" aria-controls="father" aria-selected="false">Father</button>
                </li>
            </ul>
            <div className="tab-content mt-3" id="myTabContent">
                <div className="tab-pane show active" id="you" role="tabpanel" aria-labelledby="you-tab">
                    <div className="">

                        <div className="">
                            <form >
                                <div className="row">
                                    <div className="col-12 col-md-4">
                                        <div className="mb-3">
                                            <label for="" className="col-form-label pb-0">Full Name</label>
                                            <div className="">
                                                <span className="form-control-plaintext text-secondary pt-0">
                                                    Netra Anant Ghag
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="mb-3">
                                            <label for="" className="col-form-label pb-0">Height</label>
                                            <div className="">
                                                <span className="form-control-plaintext text-secondary pt-0">
                                                    5'1
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="mb-3">
                                            <label for="" className="col-form-label pb-0">Weight</label>
                                            <div className="">
                                                <span className="form-control-plaintext text-secondary pt-0">
                                                    50
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="mb-3">
                                            <label for="" className="col-form-label pb-0">Gender</label>
                                            <div className="">
                                                <span className="form-control-plaintext text-secondary pt-0">
                                                    Female
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="mb-3">
                                            <label for="" className="col-form-label pb-0">Date of Birth</label>
                                            <div className="">
                                                <span className="form-control-plaintext text-secondary pt-0">
                                                    31/10/1991
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="mb-3">
                                            <label for="" className="col-form-label pb-0">Relationship</label>
                                            <div className="">
                                                <span className="form-control-plaintext text-secondary pt-0">
                                                    31/10/1991
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="mb-3">
                                            <label for="" className="col-form-label pb-0">Profession</label>
                                            <div className="">
                                                <span className="form-control-plaintext text-secondary pt-0">
                                                    Buisness
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div className="tab-pane" id="spouse" role="tabpanel" aria-labelledby="spouse-tab">
                    <div className="">
                        <div className="">
                            <form >
                                <div className="row">
                                    <div className="col-12 col-md-4">
                                        <div className="mb-3">
                                            <label for="" className="col-form-label pb-0">Full Name</label>
                                            <div className="">
                                                <span className="form-control-plaintext text-secondary pt-0">
                                                    Netra Anant Ghag
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="mb-3">
                                            <label for="" className="col-form-label pb-0">Height</label>
                                            <div className="">
                                                <span className="form-control-plaintext text-secondary pt-0">
                                                    5'1
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="mb-3">
                                            <label for="" className="col-form-label pb-0">Weight</label>
                                            <div className="">
                                                <span className="form-control-plaintext text-secondary pt-0">
                                                    50
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="mb-3">
                                            <label for="" className="col-form-label pb-0">Gender</label>
                                            <div className="">
                                                <span className="form-control-plaintext text-secondary pt-0">
                                                    Female
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="mb-3">
                                            <label for="" className="col-form-label pb-0">Date of Birth</label>
                                            <div className="">
                                                <span className="form-control-plaintext text-secondary pt-0">
                                                    31/10/1991
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="mb-3">
                                            <label for="" className="col-form-label pb-0">Relationship</label>
                                            <div className="">
                                                <span className="form-control-plaintext text-secondary pt-0">
                                                    31/10/1991
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="mb-3">
                                            <label for="" className="col-form-label pb-0">Profession</label>
                                            <div className="">
                                                <span className="form-control-plaintext text-secondary pt-0">
                                                    Buisness
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div className="tab-pane" id="father" role="tabpanel" aria-labelledby="father-tab">
                <div className="">
                        <div className="">
                            <form >
                                <div className="row">
                                    <div className="col-12 col-md-4">
                                        <div className="mb-3">
                                            <label for="" className="col-form-label pb-0">Full Name</label>
                                            <div className="">
                                                <span className="form-control-plaintext text-secondary pt-0">
                                                     Anant Ghag
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="mb-3">
                                            <label for="" className="col-form-label pb-0">Height</label>
                                            <div className="">
                                                <span className="form-control-plaintext text-secondary pt-0">
                                                   5'8
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="mb-3">
                                            <label for="" className="col-form-label pb-0">Weight</label>
                                            <div className="">
                                                <span className="form-control-plaintext text-secondary pt-0">
                                                    65
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="mb-3">
                                            <label for="" className="col-form-label pb-0">Gender</label>
                                            <div className="">
                                                <span className="form-control-plaintext text-secondary pt-0">
                                                    Male
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="mb-3">
                                            <label for="" className="col-form-label pb-0">Date of Birth</label>
                                            <div className="">
                                                <span className="form-control-plaintext text-secondary pt-0">
                                                    02/06/1962
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="mb-3">
                                            <label for="" className="col-form-label pb-0">Relationship</label>
                                            <div className="">
                                                <span className="form-control-plaintext text-secondary pt-0">
                                                    Father
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="mb-3">
                                            <label for="" className="col-form-label pb-0">Profession</label>
                                            <div className="">
                                                <span className="form-control-plaintext text-secondary pt-0">
                                                    Buisness
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    )
}
export default InsuredMemberSummary