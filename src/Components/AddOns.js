import React from 'react'

function AddOns() {
    return (
        <div>
            <div>
                <ul className="list-group shadow">
                    <li className="list-group-item">
                        <h5>Additional Search</h5>
                    </li>
                    <li className="list-group-item">
                        <input className="form-check-input me-1" type="checkbox" value="" aria-label="..." />
                        Automatic (For best quotes)
                    </li>
                    <li className="list-group-item">
                        <input className="form-check-input me-1" type="checkbox" value="" aria-label="..." />
                        Set your IDV
                    </li>
                    <li className="list-group-item">
                        <input className="form-check-input me-1" type="checkbox" value="" aria-label="..." />
                        Passenger Cover
                    </li>
                    <li className="list-group-item">
                        <input className="form-check-input me-1" type="checkbox" value="" aria-label="..." />
                        PA Owner Driver Cover
                    </li>
                    <li className="list-group-item">
                        <input className="form-check-input me-1" type="checkbox" value="" aria-label="..." />
                        TPPD Restricted
                    </li>
                </ul>
            </div>
            <div className="mt-2">
                <ul className="list-group shadow">
                    <li className="list-group-item">
                        <h5>Additional Cover</h5>
                    </li>
                    <li className="list-group-item">
                        <input className="form-check-input me-1" type="checkbox" value="" aria-label="..." />
                        Zero Depreciation
                    </li>
                    <li className="list-group-item">
                        <input className="form-check-input me-1" type="checkbox" value="" aria-label="..." />
                        24x7 Road Side Assistance
                    </li>
                </ul>
            </div>
        </div>
    )
}
export default AddOns;
