import React, { Component } from 'react'
import MotorSummary from '../MotorSummary'
import HealthSummary from '../HealthSummary'

function Summary() {
  
    return (
        <div>
            <div className="container">
                <div className="row">
                    <div className="col-12 col-md-8">
                      
                    <div>
                            {/* <MotorSummary/> */}
                             <HealthSummary/>
                        </div>

                    </div>
                    <div className="col-12 col-md-4">
                        <div className="bg-light mb-4 shadow">
                            <div className="p-3">
                                <h5>Comprehensive - Standard(3+1)</h5>
                                <p className="mb-0">For your Honda City 2021</p>
                            </div>
                        </div>
                        <div className="mt-3">
                            <a className="list-group-item border-0" data-bs-toggle="collapse" href="#productDetails" role="button" aria-expanded="false" aria-controls="productDetails">
                                <h5>Comprehensive - Standard(3+1)</h5>
                            </a>
                            <ol className="list-group shadow" id="productDetails">

                                <li className="list-group-item d-flex justify-content-between align-items-start border-0">
                                    <div className="ms-2 me-auto">
                                        Om Damage Premium
                                    </div>
                                    <span className="">6533</span>
                                </li>
                                <li className="list-group-item d-flex justify-content-between align-items-start border-0">
                                    <div className="ms-2 me-auto">
                                        Third Party Premium
                                    </div>
                                    <span className="">9534</span>
                                </li>
                                <li className="list-group-item d-flex justify-content-between align-items-start border-0">
                                    <div className="ms-2 me-auto">
                                        Consumables Premium
                                    </div>
                                    <span className="">220</span>
                                </li>
                                <li className="list-group-item d-flex justify-content-between align-items-start border-0">
                                    <div className="ms-2 me-auto">
                                        Net Premium
                                    </div>
                                    <span className="">16587</span>
                                </li>
                                <li className="list-group-item d-flex justify-content-between align-items-start border-0">
                                    <div className="ms-2 me-auto">
                                        GST
                                    </div>
                                    <span className="">2985</span>
                                </li>
                                <li className="list-group-item d-flex justify-content-between align-items-start border-0 bg-light ">
                                    <div className="ms-2 m-auto fw-bold">
                                        Total
                                    </div>
                                    <span className="h5 ms-auto my-auto fw-bold">1953</span>
                                </li>
                            </ol>
                        </div>
                        <div className="d-grid gap-2">
                            <button className="btn btn-primary" type="button">Make Payment</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    )

}

export default Summary
