import React from "react";

const Fields = props => {
    return (
        <>

            <div className="mb-3">
                <lable htmlFor={props.label} className="form-label" >{props.label}</lable>
                <input defaultValue={props.value} type={props.type} className="form-control" id={props.id} placeholder={props.placeholder}></input>
            </div>
        </>
    )
}

export default Fields