import React, { useState } from "react";
import Fields from "./Fields";

const Login = () => {
    const [mobile, setMobile] = useState("");
    const [password, setPassword] = useState("");
    const [toggleState, setToggleState] = useState(1);
    const toggleTab = (index) => {
        setToggleState(index);
    }

    return (
        <>
            <div>
                <div className="text-center">
                    <button onClick={() => toggleTab(1)} className={toggleState === 1 ? "btn btn-primary" : " btn btn-secondary"}>Login</button>
                    <button onClick={() => toggleTab(2)} className={toggleState === 2 ? "btn btn-primary" : "btn btn-secondary"}>Sign Up </button>
                </div>
                {/* login form */}
                <form className={toggleState === 1 ? "d-block" : "d-none"}>
                    <section className=" gradient-custom">
                        <div className="container py-5 h-100">
                            <div className="row d-flex justify-content-center align-items-center h-100">
                                <div className="col-12 col-md-8 col-lg-6 col-xl-5">
                                    <div className="card bg-white text-dark">
                                        <div className="card-body p-5 ">

                                            <div className="mb-md-5 mt-md-4 pb-2">

                                                <h2 className="fw-bold mb-2 text-uppercase">Login</h2>
                                                <p className="text-dark-50 mb-5">Please enter your login and password!</p>

                                                <div className="form-outline form-white mb-4">
                                                    <Fields label="Mobile" type="text" placeholder="Enter Mobile Number" Value={mobile}
                                                        onChange={(e) => setMobile(e.target.value)} />
                                                </div>

                                                <div className="form-outline form-white mb-4">
                                                    <Fields label="Password" type="password" placeholder="Enter Password" value={password}
                                                        onChange={(e) => setPassword(e.target.value)} />
                                                </div>

                                                <p className="small mb-1 pb-lg-2"><a className="text-dark-50" href="#!">Forgot password?</a></p>

                                                <button className="btn btn-outline-primary btn-lg px-5" type="submit">Login</button>


                                            </div>

                                            <div>
                                                <p className="mb-0">Don't have an account? <a href="#!" className="text-dark fw-bold">Sign Up</a></p>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </form>

                {/* sign up form */}
                <form className={toggleState === 2 ? "d-block" : "d-none"}>
                    <section className=" gradient-custom">
                        <div className="container py-5 h-100">
                            <div className="row d-flex justify-content-center align-items-center h-100">
                                <div className="col-12 col-md-8 col-lg-6 col-xl-5">
                                    <div className="card bg-white text-dark">
                                        <div className="card-body p-5 ">

                                            <div className="mb-md-5 mt-md-4 pb-2">

                                                <h2 className="fw-bold mb-2 text-uppercase">Sign Up</h2>
                                                <p className="text-dark-50 mb-5">Please enter your login and password!</p>

                                                <div className="form-outline form-white mb-4">
                                                    <Fields label="Mobile" type="text" placeholder="Enter Mobile Number" Value={mobile}
                                                        onChange={(e) => setMobile(e.target.value)} />
                                                </div>

                                                <div className="form-outline form-white mb-4">
                                                    <Fields label="Email" type="email" placeholder="Enter Email ID" value={password}
                                                        onChange={(e) => setPassword(e.target.value)} />
                                                </div>

                                                <p className="small mb-1 pb-lg-2"><a className="text-dark-50" href="#!">Forgot password?</a></p>

                                                <button className="btn btn-outline-primary btn-lg px-5" type="submit">Sign Up</button>


                                            </div>

                                            <div>
                                                <p className="mb-0">Don't have an account? <a href="#!" className="text-dark fw-bold">Login</a></p>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </form>
            </div>
        </>
    )
};
export default Login;