import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ProposerDetails from '../ProposerDetails'
import NomineeDetails from '../NomineeDetails'
import MedicalHistory from '../MedicalHistory'
import InsuredMember from '../InsuredMember'
import { Link } from 'react-router-dom'

function ProposalForm() {
    const [count, setCount] = React.useState(0);
    const components = [
        <div>
            <ProposerDetails />
        </div>,
        <div>
            <InsuredMember/>
        </div>,
        <div>
            <NomineeDetails />
        </div>,
        <div>
                <MedicalHistory/>
        </div>
    ]
    return (
        <div>
            <div className="container">
                <div className="row">
                    <div className="col-12 col-md-8">
                        {
                            // render component from our components array
                            components[count]
                        }


                        <div className="mt-3 text-end">
                            {/* <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="button" className="btn btn-primary">Save changes</button> */}
                            {/* show previous button if we are not on first element */}
                            {count > 0 && <button onClick={() => setCount(count - 1)} className="btn btn-primary">Prev</button>}

                            {/* hide next button if we are at the last element */}
                            {count < components.length - 1 && <button onClick={() => setCount(count + 1)} className="btn btn-primary ms-2">Next</button>}

                            {/* {count > components.length - 1 && <Link to="" className="btn btn-primary">finish</Link>} */}
                            
                        </div>
                    </div>
                    <div className="col-12 col-md-4">
                        <div className="bg-light mb-4 shadow">
                            <div className="p-3">
                                <h5>Comprehensive - Standard(3+1)</h5>
                                <p className="mb-0">For your Honda City 2021</p>
                            </div>
                        </div>
                        <div className="mt-3">
                            <a className="list-group-item border-0" data-bs-toggle="collapse" href="#productDetails" role="button" aria-expanded="false" aria-controls="productDetails">
                                <h5>Comprehensive - Standard(3+1)</h5>
                            </a>
                            <ol className="list-group shadow" id="productDetails">

                                <li className="list-group-item d-flex justify-content-between align-items-start border-0">
                                    <div className="ms-2 me-auto">
                                        Om Damage Premium
                                    </div>
                                    <span className="">6533</span>
                                </li>
                                <li className="list-group-item d-flex justify-content-between align-items-start border-0">
                                    <div className="ms-2 me-auto">
                                        Third Party Premium
                                    </div>
                                    <span className="">9534</span>
                                </li>
                                <li className="list-group-item d-flex justify-content-between align-items-start border-0">
                                    <div className="ms-2 me-auto">
                                        Consumables Premium
                                    </div>
                                    <span className="">220</span>
                                </li>
                                <li className="list-group-item d-flex justify-content-between align-items-start border-0">
                                    <div className="ms-2 me-auto">
                                        Net Premium
                                    </div>
                                    <span className="">16587</span>
                                </li>
                                <li className="list-group-item d-flex justify-content-between align-items-start border-0">
                                    <div className="ms-2 me-auto">
                                        GST
                                    </div>
                                    <span className="">2985</span>
                                </li>
                                <li className="list-group-item d-flex justify-content-between align-items-start border-0 bg-light ">
                                    <div className="ms-2 m-auto fw-bold">
                                        Total
                                    </div>
                                    <span className="h5 ms-auto my-auto fw-bold">1953</span>
                                </li>
                            </ol>
                        </div>
                        {/* <div className="d-grid gap-2">
                            <button className="btn btn-primary" type="button">Make Payment</button>
                        </div> */}
                    </div>

                </div>
            </div>
        </div>
    )

}

export default ProposalForm
