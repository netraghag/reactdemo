import React from "react";
import { Link } from "react-router-dom";

const Header = () => {
    return(
        <>
            <nav
            className="navbar navbar-expand-lg fixed-top navbar-light cust-main-navbar mx-auto px-2 px-md-4 pt-auto pt-md-3 bg-white">
            <Link className="navbar-brand" to={`/`}>
               Logo
            </Link>
            <button className="navbar-toggler border-0" type="button" data-bs-toggle="collapse" data-bs-target="#nav" aria-controls="navbarNav"
                aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse justify-content-end" id="nav">
                <ul className="navbar-nav m-0 p-3 p-lg-0">
                    <li className="d-inline d-lg-none">
                        <button data-bs-toggle="collapse" data-bs-target="#nav" className="close float-right  btn px-0 text-danger"><span className="navbar-toggler-icon"></span></button>
                    </li>
                    <li className="nav-item pl-4 pl-md-0 me-0 me-md-5">
                        <Link className="nav-link text-truncate nav-menu" to={`/`}>
                            <span className="ms-2 ms-lg-0">Home</span>
                        </Link>
                    </li>
                    <li className="nav-item pl-4 pl-md-0 me-0 me-md-5">
                        <Link className="nav-link text-truncate nav-menu" to={`/login`}>
                            <span className="ms-2 ms-lg-0">Login</span>
                        </Link>
                    </li>
                    <li className="nav-item pl-4 pl-md-0 me-0 justify-content-end">
                        <Link className="nav-link text-truncate btn cust-btn-warning text-white" to={`/getQuote`}>Get quote</Link>
                    </li>
                </ul>
            </div>
        </nav>
        </>
    )
}

export default Header