import React from 'react'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { useState } from 'react/cjs/react.development';
function EnquiryPage() {
    const [count, setCount] = React.useState(0);
    const [formData, setformData] = useState({
        ExpirePolicy : "no"
    })
    const handleChange = event =>{
        const target = event.target
        const name = target.name
        const value = target.value
        setformData({
                ...formData,
                [name] : value
        })
    }

    const components = [
        <div> <div className="container px-0">
            <div className="row g-1">

                <div className="col-3"><div className="border text-center" roll="button">
                    <img src="https://freepikpsd.com/media/2020/01/maruti-suzuki-logo-design-india-PNG-Transparent-Images.png" className="img-fluid" /> <p>Name</p></div>
                </div>
                <div className="col-3"><div className="border text-center" roll="button">
                    <img src="https://freepikpsd.com/media/2020/01/maruti-suzuki-logo-design-india-PNG-Transparent-Images.png" className="img-fluid" /> <p>Name</p></div>
                </div>
                <div className="col-3"><div className="border text-center" roll="button">
                    <img src="https://freepikpsd.com/media/2020/01/maruti-suzuki-logo-design-india-PNG-Transparent-Images.png" className="img-fluid" /> <p>Name</p></div>
                </div>
                <div className="col-3"><div className="border text-center" roll="button">
                    <img src="https://freepikpsd.com/media/2020/01/maruti-suzuki-logo-design-india-PNG-Transparent-Images.png" className="img-fluid" /> <p>Name</p></div>
                </div>


            </div>
        </div>
        </div>,
        <div>
            <div className="conrainer">
                <div className="row g-1">
                    <div className="col-3"><div className="border text-center py-2">ES</div></div>
                    <div className="col-3"><div className="border text-center py-2">LC</div></div>
                    <div className="col-3"><div className="border text-center py-2">LS</div></div>
                    <div className="col-3"><div className="border text-center py-2">LX</div></div>
                    <div className="col-3"><div className="border text-center py-2">ES</div></div>
                    <div className="col-3"><div className="border text-center py-2">LC</div></div>
                    <div className="col-3"><div className="border text-center py-2">LS</div></div>
                    <div className="col-3"><div className="border text-center py-2">LX</div></div>
                </div>
            </div>
        </div>,
        <div>
            <div className="conrainer">
                <div className="row g-1">
                    <div className="col-12"><div className="border text-center py-2">PETROL</div></div>
                    <div className="col-12"><div className="border text-center py-2">DIESEL</div></div>
                    <div className="col-12"><div className="border text-center py-2">EXTERNAL FITTED CNG </div></div>
                    <div className="col-12"><div className="border text-center py-2">EXTERNAL FITTED LPG</div></div>
                </div>
            </div>
        </div>,
        <div>
            <div className="conrainer">
                <div className="row g-1">
                    <div className="col-12"><div className="border text-center py-2">1.4 MAGNA DIESEL (1396CC)</div></div>
                    <div className="col-12"><div className="border text-center py-2">1.4 SPORTZ AT (1396CC)</div></div>
                    <div className="col-12"><div className="border text-center py-2">1.4 SPORTZ AT (1396CC) </div></div>
                    <div className="col-12"><div className="border text-center py-2">1.4L ASTA AT WITH AVN (1396CC)</div></div>
                </div>
            </div>
        </div>
    ]

    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-12 col-lg-6">
                    <div className="text-center">
                        <h2>Car Insurance</h2>
                        <small>COMPARE & BUY AFFORDABLE TERM INSURANCE PLANS AT LOWEST RATES</small>
                    </div>
                    <div>
                        <form className="row g-3 mt-3">
                            <div className="col-md-8">
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" />
                                    <label className="form-check-label" for="inlineRadio1">T.P. Only(3 Yrs)</label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2" />
                                    <label className="form-check-label" for="inlineRadio2">Comprehensive(1 Yr) + T.P.(2 Yrs)</label>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <select id="inputState" className="form-select my-auto">
                                    <option selected>Individual</option>
                                    <option>Company</option>
                                </select>
                            </div>
                            <div className="col-12">
                                <label for="inputAddress" className="form-label">Vehicle Make, Model & Varient</label>
                                <a role="button" data-bs-toggle="modal" data-bs-target="#exampleModal"><input type="text" className="form-control" id="inputAddress" placeholder="1234 Main St" /></a>
                            </div>
                            <div className="col-6">
                                <label for="inputAddress" className="form-label">Place of Registration</label>
                                <input type="text" className="form-control" id="inputAddress" placeholder="Enter Place" />
                            </div>
                            <div className="col-6">
                                <label for="inputAddress2" className="form-label">Date of Registration</label>
                                <input type="date" className="form-control" id="inputAddress2" placeholder="" />
                            </div>
                            {/* <div className="col-md-6">
                                <label for="inputName" className="form-label">Full Name</label>
                                <input type="text" className="form-control" id="inputName" placeholder="Enter Full Name" />
                            </div> */}
                            <div className="col-12">
                                <div className="d-flex">
                                    <label for="inputAddress2" className="form-label">Has Your Policy Expired</label>
                                    <div className="ms-3">
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="ExpirePolicy" id="radioYes" value="yes" onChange={handleChange} checked={formData.ExpirePolicy=="yes"}/>
                                            <label className="form-check-label" for="radioYes">Yes</label>
                                        </div>
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="ExpirePolicy" id="radioNo" value="no" onChange={handleChange} checked={formData.ExpirePolicy=="no"}/>
                                            <label className="form-check-label" for="radioNo">No</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className={formData === yes ? "d-none" : "d-block"}>
                            <div className="col-6">
                            <label for="inputAddress2" className="form-label">Previous Insurer</label>
                                <select id="previousInsurer" className="form-select my-auto">
                                    <option selected>Raheja QBE General Insurance</option>
                                    <option>Reliance General Insurance</option>
                                </select>
                            </div>
                            <div className="col-6">
                                <label for="policyExpired" className="form-label">Policy Expiry Date</label>
                                <input type="date" className="form-control" id="policyExpired" placeholder="" />
                            </div>
                            <div className="col-12">
                            <div className="d-flex mt-3">
                                    <label for="inputAddress2" className="form-label">Claim Made Last Year?</label>
                                    <div className="ms-3">
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="ExpirePolicy" id="claimYes" value="option1" />
                                            <label className="form-check-label" for="claimYes">Yes</label>
                                        </div>
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="ExpirePolicy" id="claimNo" value="option2" />
                                            <label className="form-check-label" for="claimNo">No</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div className="col-6">
                                <label for="inputAddress2" className="form-label">Enter Amount</label>
                                <input type="text" className="form-control" id="inputAddress2" placeholder="Min Amount: 10,000 Max Amount: 50,000" />
                            </div>

                            <div className="col-md-6">
                                <label for="inputMobile" className="form-label">Mobile</label>
                                <input type="number" className="form-control" id="inputMobile" placeholder="Enter Your Contact Details" />
                            </div>
                            {/* <div className="col-md-12">
                                <label for="inputEmail4" className="form-label">Email</label>
                                <input type="email" className="form-control" id="inputEmail4" placeholder="Enter Email Address" />
                            </div> */}
                            <div className="d-grid gap-2">
                           <button className="btn btn-primary custom-btn btn-13">Submit</button>
                            </div>
                            <div className="text-center">
                                <a>I DON'T REMEMBER MY VEHICLE NO.</a>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            {/* <!-- Modal --> */}
            <div className="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog">

                    <div className="modal-content bg-transparent border-0">
                        <div className="bg-white mb-3 card">
                            <form className="col-12  mb-2 mb-lg-0 me-lg-auto">
                                <input type="search" className="form-control" placeholder="Search here..." aria-label="Search" />
                            </form>
                        </div>
                        <div className="modal-header bg-white">
                            <h5 className="modal-title" id="exampleModalLabel">Vehicle Make</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body bg-white">
                            <div>
                                {
                                    // render component from our components array
                                    components[count]
                                }


                            </div>

                        </div>
                        <div className="modal-footer bg-white">
                            {/* <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="button" className="btn btn-primary">Save changes</button> */}
                            {/* show previous button if we are not on first element */}
                            {count > 0 && <button onClick={() => setCount(count - 1)} className="btn btn-primary">Prev</button>}

                            {/* hide next button if we are at the last element */}
                            {count < components.length - 1 && <button onClick={() => setCount(count + 1)} className="btn btn-primary">Next</button>}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default EnquiryPage;