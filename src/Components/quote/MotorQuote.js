import React from 'react'
import Shimmer from "react-shimmer-effect";
import AddOns from "../AddOns";
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faAngleDown } from '@fortawesome/free-solid-svg-icons'

function MotorQuote() {
    return (

        <div>

            <div className="bg-white text-center shadow ">
                <span className="text-warning">Important:</span> As per IRDAI directive, it is mandatory to buy 5 year Insurance Cover (3rd Party) <span className="text-warning">for new Two Wheelers.</span>
            </div>
     
            <div className="container policy_card__listing">
                <div className="my-3">
                    <Link to="/motor-enquiry-page" className="btn btn-sm btn-primary">Back to Quote</Link>
                </div>
                <div className="card shadow mb-3">
                    <div className="row p-2">
                        <div className="col-md-3 border-end ">
                            <div className="d-flex">Vehical Model
                                <span className="ms-auto">  <FontAwesomeIcon icon={faEdit}></FontAwesomeIcon></span>
                            </div>
                            <div className="fw-bold text-custom-color">AJANTA OREVA</div>
                        </div>
                        <div className="col-md-3 border-end">
                            <div className="d-flex">Registered Rto <span className="ms-auto">  <FontAwesomeIcon icon={faEdit}></FontAwesomeIcon></span></div>

                            <div className="fw-bold">HR53</div>
                        </div>
                        <div className="col-md-3 border-end">
                            <div className="d-flex">Purchase Date
                                <span className="ms-auto">  <FontAwesomeIcon icon={faEdit}></FontAwesomeIcon></span>
                            </div>
                            <div className="fw-bold">31 Aug 2021</div>
                        </div>
                        <div className="col-md-3">
                            <div className="d-flex">Previous Insurer
                                <span className="ms-auto">  <FontAwesomeIcon icon={faEdit}></FontAwesomeIcon></span>
                            </div>
                            <div className="fw-bold">Select Insurer</div>
                        </div>
                    </div>
                </div>
                <div className="row mt-5">
                    <div className="col-12 col-md-4 order-1">
                        <div className="bg-light mb-4 shadow">
                            <div className="p-3">
                                <h5>Comprehensive - Standard(3+1)</h5>
                                <p className="mb-0">For your Honda City 2021</p>
                            </div>
                        </div>
                        <div className="mt-3">
                            <AddOns/>
                        </div>
                        <div className="mt-3">
                            <a className="list-group-item" data-bs-toggle="collapse" href="#productDetails" role="button" aria-expanded="false" aria-controls="productDetails">
                                <div className="d-flex">
                                    <h5>Comprehensive - Standard(3+1)</h5>
                                    <span className="ms-auto my-auto"><FontAwesomeIcon icon={faAngleDown}></FontAwesomeIcon></span>
                                </div>

                            </a>
                            <ol className="list-group shadow " id="productDetails">

                                <li className="list-group-item d-flex justify-content-between align-items-start">
                                    <div className="ms-2 me-auto">
                                        Om Damage Premium
                                    </div>
                                    <span className="">6533</span>
                                </li>
                                <li className="list-group-item d-flex justify-content-between align-items-start">
                                    <div className="ms-2 me-auto">
                                        Third Party Premium
                                    </div>
                                    <span className="">9534</span>
                                </li>
                                <li className="list-group-item d-flex justify-content-between align-items-start">
                                    <div className="ms-2 me-auto">
                                        Consumables Premium
                                    </div>
                                    <span className="">220</span>
                                </li>
                                <li className="list-group-item d-flex justify-content-between align-items-start">
                                    <div className="ms-2 me-auto">
                                        Net Premium
                                    </div>
                                    <span className="">16587</span>
                                </li>
                                <li className="list-group-item d-flex justify-content-between align-items-start">
                                    <div className="ms-2 me-auto">
                                        GST
                                    </div>
                                    <span className="">2985</span>
                                </li>
                                <li className="list-group-item d-flex justify-content-between align-items-start bg-light">
                                    <div className="ms-2 me-auto">
                                        Total
                                    </div>
                                    <span className="h5">1953</span>
                                </li>
                            </ol>
                        </div>
                        {/* <div className="d-grid gap-2 mt-3">
                            <button className="btn btn-warning" type="button">Download Policy Wording</button>
                        </div> */}



                    </div>
                    <div className="col-12 col-md-8 order-2">
                        <div className="mb-3 bg-light">
                            <div className="d-flex">
                                <div className="d-flex">
                                    Select Multi Year OD?
                                    <div className="ms-3">
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" />
                                            <label className="form-check-label" for="inlineRadio1">1 Year</label>
                                        </div>
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2" />
                                            <label className="form-check-label" for="inlineRadio2">3 Years</label>
                                        </div>
                                    </div>
                                </div>
                                <div className="ms-auto d-flex">
                                    <button className="me-2 badge bg-success " data-bs-toggle="modal" data-bs-target="#staticBackdrop">share</button>
                                    <select className="form-select form-select-sm" aria-label=".form-select-sm example">

                                        <option value="1">Premium</option>
                                        <option value="2">IDV</option>
                                    </select></div>
                            </div>
                        </div>
                        <div className="accordion" id="accordionPanelsStayOpenExample">
                            <div className="accordion-item  shadow">
                                <div className="text-end ">
                                    <input type="checkbox" className="form-check-input mt-2 me-2" />
                                </div>
                                <div className="container py-4">
                                    <div className="row">
                                        <div className="col-12 col-md-4">
                                            <img src="https://app.turtlemint.com/images/TATA.png" alt="" />
                                            <h6>MediCare Premier</h6>
                                        </div>
                                        <div className="col-12 col-md-4">
                                            <ul>
                                                <li>
                                                    Cover : 5.0 Lakhs
                                                </li>
                                                <li>
                                                    Bonus / NCB :
                                                    50% for every claim free year, max upto Sum insured
                                                </li>
                                                <li>
                                                    Copay :
                                                    No copayment

                                                </li>
                                                <li>
                                                    Claims Settled :
                                                    93.08%
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="col-12 col-md-4 text-center">
                                            <Link to="/proposal-form" className="btn btn-primary">9,867</Link>
                                            <p>For 1 yr</p>
                                        </div>
                                    </div>
                                </div>
                                <h2 className="accordion-header" id="panelsStayOpen-headingOne">
                                    <button className="accordion-button bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
                                        Why is this the best plan for you ?
                                    </button>
                                </h2>
                                <div id="panelsStayOpen-collapseOne" className="accordion-collapse collapse" aria-labelledby="panelsStayOpen-headingOne">
                                    <div className="accordion-body">
                                        <strong>This is the first item's accordion body.</strong> It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                                    </div>
                                </div>
                            </div>
                            <div className="accordion-item shadow mt-4">
                                <div className="container py-4">
                                    <div className="row">
                                        <div className="col-12 col-md-4">
                                            <img src="https://app.turtlemint.com/images/TATA.png" alt="" />
                                            <h6>MediCare Premier</h6>
                                        </div>
                                        <div className="col-12 col-md-4">
                                            <ul>
                                                <li>
                                                    Cover : 5.0 Lakhs
                                                </li>
                                                <li>
                                                    Bonus / NCB :
                                                    50% for every claim free year, max upto Sum insured
                                                </li>
                                                <li>
                                                    Copay :
                                                    No copayment

                                                </li>
                                                <li>
                                                    Claims Settled :
                                                    93.08%
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="col-12 col-md-4 text-center">
                                            <button className="btn btn-primary">9,867</button>
                                            <p>For 1 yr</p>
                                        </div>
                                    </div>
                                </div>
                                <h2 className="accordion-header" id="panelsStayOpen-headingTwo">
                                    <button className="accordion-button collapsed bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="false" aria-controls="panelsStayOpen-collapseTwo">
                                        Why is this the best plan for you ?
                                    </button>
                                </h2>
                                <div id="panelsStayOpen-collapseTwo" className="accordion-collapse collapse" aria-labelledby="panelsStayOpen-headingTwo">
                                    <div className="accordion-body">
                                        <strong>This is the second item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                                    </div>
                                </div>
                            </div>
                            <div className="accordion-item shadow mt-4">
                                <div className="container py-2">
                                    <div className="row">
                                        <div className="col-12 col-md-4 d-flex">

                                            <img src="https://app.turtlemint.com/images/TATA.png" alt="" />
                                            <div>
                                                <h6 className="fw-bold">MediCare Premier</h6>
                                                <small className="">Insured Declared Value<br />(IDV)</small>
                                            </div>
                                        </div>
                                        <div className="col-12 col-md-4">
                                            <ul className="list-unstyled">
                                                <li>
                                                    <h5 className="text-primary fw-bold">Rs.5899</h5>
                                                    <a className="accordion-button collapsed p-0 bg-light" roll="button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseThree" aria-expanded="false" aria-controls="panelsStayOpen-collapseThree">
                                                        Premium Details
                                                    </a>
                                                </li>

                                            </ul>
                                        </div>
                                        <div className="col-12 col-md-4 text-center">
                                            <button className="btn btn-primary">BUY</button>
                                            <p>For 1 yr</p>

                                        </div>
                                    </div>
                                </div>
                                <div id="panelsStayOpen-collapseThree" className="accordion-collapse collapse" aria-labelledby="panelsStayOpen-headingThree">
                                    <div className="accordion-body">
                                        <div className="row border-top ">
                                            <div className="col-6 mt-3">
                                                <ol className="list-group list-group-flush">
                                                    <li className="list-group-item d-flex justify-content-between align-items-start">
                                                        <div className="ms-2 me-auto">
                                                            No Claim Discount
                                                        </div>
                                                        <span className="">0%</span>
                                                    </li>
                                                    <li className="list-group-item d-flex justify-content-between align-items-start">
                                                        <div className="ms-2 me-auto">
                                                            Own-Damage Premium
                                                        </div>
                                                        <span className="">14</span>
                                                    </li>
                                                    <li className="list-group-item d-flex justify-content-between align-items-start">
                                                        <div className="ms-2 me-auto">
                                                            PA Owner Driver Premium
                                                        </div>
                                                        <span className="">14</span>
                                                    </li>
                                                    <li className="list-group-item d-flex justify-content-between align-items-start">
                                                        <div className="ms-2 me-auto">
                                                            <div className="fw-bold">Total</div>
                                                        </div>
                                                        <span className="fw-bold h5">14,152</span>
                                                    </li>
                                                </ol>
                                            </div>
                                            <div className="col-6 mt-3 border-start" >
                                                <div className="d-flex">
                                                    <span>Insured Declared Value (IDV)Rs.</span>
                                                    <span className="ms-auto fw-bold">7,17,232</span>
                                                </div>
                                                <div className="mt-2">
                                                    <h6 className="fw-bold">Policy Coverage</h6>
                                                    <ul>
                                                        <li>Theft or accidental damage</li>
                                                        <li>Third Party Liability</li>
                                                        <li>Zero Depreciation</li>
                                                        <li>Normal wear & tear, mechanical breakdown</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {/* pincinsurance */}
                        <div className="mt-3">
                            <div className="bg-white">
                                <div className=" shadow mt-4">
                                    <div className="container py-2">
                                        <div className="row">
                                            <div className="col-12 col-md-3 ">

                                                <img src="https://app.turtlemint.com/images/TATA.png" alt="" />
                                                <div>
                                                    <h6 className="fw-bold">MediCare Premier</h6>
                                                    <small className="">Insured Declared Value<br />(IDV)</small>
                                                </div>
                                            </div>
                                            <div className="col-12 col-md-2">
                                                IDV
                                                <p> Rs.3,01,140</p>
                                            </div>
                                            <div className="col-12 col-md-5">
                                                <p className=" mb-0 cust-fs-14 d-flex">Addons <span className="ms-auto" data-toggle="modal" data-target="#addOns"> + Add</span></p>

                                                <div className=" addons-list cust-addons">
                                                    <ul className="list-unstyled w-100 ">
                                                        <li className="text-success fw-bold">Zero Depreciation</li>
                                                        <li className="text-success fw-bold ">24x7 Road Side Assistance</li>
                                                        <li className="text-success fw-bold ">Engine Protection Cover</li>
                                                        <li className="text-success fw-bold ">NCB Protector</li>
                                                        <li className="text-success fw-bold ">Key &amp; Lock Replacement</li>
                                                        <li className="text-secondary"><small>X Invoice Price</small></li>
                                                        <li className="text-secondary"><small>X Tyre Secure</small></li>
                                                        <li className="text-secondary "><small>X Driver Cover</small></li>
                                                    </ul>
                                                </div>


                                            </div>

                                            <div className="col-12 col-md-2 text-center">
                                                <button className="btn btn-primary">Rs.5899</button>
                                                <p>Breakup</p>

                                            </div>
                                        </div>
                                    </div>
                                    {/* collapsed Start */}
                                    {/* <div className="px-2 bg-light">
                                    <a className="accordion-button collapsed py-2  mt-3 bg-transparent" roll="button" type="button" data-bs-toggle="collapse" data-bs-target="#panelCard" aria-expanded="false" aria-controls="panelCard">
                                                    Premium Details
                                                </a>
                                    </div>
                                    <div id="panelCard" className="accordion-collapse collapse" aria-labelledby="panelCard">
                                        <div className="accordion-body">
                                            <div className="row border-top ">
                                                <div className="col-6 mt-3">
                                                    <ol className="list-group list-group-flush">
                                                        <li className="list-group-item d-flex justify-content-between align-items-start">
                                                            <div className="ms-2 me-auto">
                                                                No Claim Discount
                                                            </div>
                                                            <span className="">0%</span>
                                                        </li>
                                                        <li className="list-group-item d-flex justify-content-between align-items-start">
                                                            <div className="ms-2 me-auto">
                                                                Own-Damage Premium
                                                            </div>
                                                            <span className="">14</span>
                                                        </li>
                                                        <li className="list-group-item d-flex justify-content-between align-items-start">
                                                            <div className="ms-2 me-auto">
                                                                PA Owner Driver Premium
                                                            </div>
                                                            <span className="">14</span>
                                                        </li>
                                                        <li className="list-group-item d-flex justify-content-between align-items-start">
                                                            <div className="ms-2 me-auto">
                                                                <div className="fw-bold">Total</div>
                                                            </div>
                                                            <span className="fw-bold h5">14,152</span>
                                                        </li>
                                                    </ol>
                                                </div>
                                                <div className="col-6 mt-3 border-start" >
                                                    <div className="d-flex">
                                                        <span>Insured Declared Value (IDV)Rs.</span>
                                                        <span className="ms-auto fw-bold">7,17,232</span>
                                                    </div>
                                                    <div className="mt-2">
                                                        <h6 className="fw-bold">Policy Coverage</h6>
                                                        <ul>
                                                            <li>Theft or accidental damage</li>
                                                            <li>Third Party Liability</li>
                                                            <li>Zero Depreciation</li>
                                                            <li>Normal wear & tear, mechanical breakdown</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> */}
                                    {/* collapse END */}
                                </div>
                            </div>
                        </div>

                        {/* coverfox */}
                        <div className="row mt-5 g-4">
                            <div className="col-12 col-md-4 ">
                                <div className="bg-white py-3">
                                    <div className="text-center">
                                        <img src="https://app.turtlemint.com/images/TATA.png" alt="" />
                                        <h6>MediCare Premier</h6>
                                    </div>
                                    <div className=" text-center">
                                        <ul className="list-unstyled">
                                            <li>
                                                Cover Value(IDV): <span className="fw-bold">5,23,000</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className=" text-center px-4">
                                        <button className="btn btn-outline-primary w-100">
                                            <small>Buy Now</small>
                                            <h4>9,867</h4></button>

                                    </div>
                                    <div className="text-center">
                                        <p className="mt-3"><a href="#">Plan Details </a> | <a href="#">Cashless Garages</a></p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 col-md-4 ">
                                <div className="bg-white py-3">
                                    <div className="text-center">
                                        <img src="https://app.turtlemint.com/images/TATA.png" alt="" />
                                        <h6>MediCare Premier</h6>
                                    </div>
                                    <div className=" text-center">
                                        <ul className="list-unstyled">
                                            <li>
                                                Cover Value(IDV): <span className="fw-bold">5,23,000</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className=" text-center px-4">
                                        <button className="btn btn-outline-primary w-100">
                                            <small>Buy Now</small>
                                            <h4>9,867</h4></button>

                                    </div>
                                    <div className="text-center">
                                        <p className="mt-3"><a href="#">Plan Details </a> | <a href="#">Cashless Garages</a></p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 col-md-4">
                                <div className="bg-white py-3">
                                    <div className="text-center">
                                        <img src="https://app.turtlemint.com/images/TATA.png" alt="" />
                                        <h6>MediCare Premier</h6>
                                    </div>
                                    <div className=" text-center">
                                        <ul className="list-unstyled">
                                            <li>
                                                Cover Value(IDV): <span className="fw-bold">5,23,000</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className=" text-center px-4">
                                        <button className="btn btn-outline-primary w-100">
                                            <small>Buy Now</small>
                                            <h4>9,867</h4></button>

                                    </div>
                                    <div className="text-center">
                                        <p className="mt-3"><a href="#">Plan Details </a> | <a href="#">Cashless Garages</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <div className="phone-call">
                <img src="https://cdn2.iconfinder.com/data/icons/social-messaging-ui-color-shapes-2-free/128/social-whatsapp-circle-512.png" width="50" alt="Call Now" title="Call Now" /></div>

            {/* <Modal */}
            <div className="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="staticBackdropLabel">Share</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body ">
                            <div className="d-flex justify-content-center" >
                                <div className="_share_app">
                                    <button className="btn">
                                        <span className="ic_copy">
                                            #
                                        </span>
                                        <span>Copy Link</span>
                                    </button>
                                </div>

                                <div className="_share_app ">
                                    <button className="btn">
                                        <span className="ic_copy">
                                            <img src="https://cdn2.iconfinder.com/data/icons/social-messaging-ui-color-shapes-2-free/128/social-whatsapp-circle-512.png" width="50" alt="Call Now" title="Call Now" />
                                        </span>
                                        <span>WhatsApp</span>
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


    )
}
export default MotorQuote