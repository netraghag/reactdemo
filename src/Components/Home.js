import React from "react";
import { Link } from "react-router-dom";

const Home = () => {
  return (
    <>
      <div className="py-3">
        <div className="container">
          <h3>Our Products</h3>
          <div className="row justify-content-center g-3">
            <div className="col-2 " >
              <div className="bg-dark text-dark card">
               <Link to="/motor-enquiry-page"> Car Insurance </Link>
              </div>

            </div>
            <div className="col-2 " >
              <div className="bg-dark text-white card">
                Car Insurance
              </div>

            </div>
            <div className="col-2 " >
              <div className="bg-dark text-white card">
                Car Insurance
              </div>

            </div>
            <div className="col-2 " >
              <div className="bg-dark text-white card">
                Car Insurance
              </div>

            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Home