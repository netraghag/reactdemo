import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFemale, faMale } from '@fortawesome/free-solid-svg-icons'
function NomineeDetails(){
    return(
        <div>
                <h4>Nominee Details</h4>
                            <p>In the event of the death of an Insured Person any payment due under the Policy shall become payable to the nominee as per the Policy terms and condition. The nominee must be an immediate relative of the Proposer. Nominee for any of the persons proposed to be insured shall be the Proposer.</p>

                            <div className="card">
                                <div className="card-header d-flex">
                                    Nominee 1
                                    <button className="btn btn-primary btn-sm ms-auto">
                                       + Add Nominee
                                    </button>
                                </div>
                                <div className="card-body">
                                <form >
                                    <div className="row">
                                        <div className="col-12 col-md-6">
                                            <div>
                                                <label for="nameField" className="form-label">Full Name</label>
                                                <input type="text" className="form-control" id="nameField" placeholder="Enter name" />
                                                <span className="small text-danger">
                                                    You must agree before submitting.
                                                </span>
                                            </div>
                                        </div>
                                        <div className="col-12 col-md-6 ">
                                            <label for="inputRelation" className="form-label">Marital Status</label>
                                            <select id="inputRelation" className="form-select">
                                                <option selected>Choose...</option>
                                                <option>Single</option>
                                            </select>
                                        </div >
                                        <div className="col-12 col-md-6 mt-3">
                                            <div>
                                                <label for="nameField" className="form-label">Gender</label>
                                                <div className="genderFields">
                                                    <div className="radio-toolbar">
                                                        <input type="radio" id="radioApple" name="radioFruit" value="apple" checked />
                                                        <label for="radioApple">
                                                            <FontAwesomeIcon icon={faMale}></FontAwesomeIcon> Male</label>

                                                        <input type="radio" id="radioBanana" name="radioFruit" value="banana" />
                                                        <label for="radioBanana">
                                                            <FontAwesomeIcon icon={faFemale}></FontAwesomeIcon> Female</label>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-12 col-md-6 mt-3">
                                            <label for="inputDob" className="form-label">Date of Birth</label>
                                            <input type="date" className="form-control" id="iinputDob" />
                                        </div>
                                        <div className="col-12 col-md-6 mt-3">
                                            <label for="inputRelation" className="form-label">Relationship</label>
                                            <select id="inputRelation" className="form-select">
                                                <option selected>Choose...</option>
                                                <option>Self</option>
                                            </select>
                                        </div>
                                        <div className="col-12 col-md-6 mt-3">
                                            <label for="nationality" className="form-label">Nationality</label>
                                            <input value="Indian" type="text" className="form-control" id="nationality" disabled readOnly />
                                        </div>
                                        <div className="col-12 col-md-6 mt-3">
                                            <label for="annualIncome" className="form-label">Annual Income</label>
                                            <input  type="text" className="form-control" id="annualIncome" />
                                        </div>
                                        <div className="col-12 col-md-6 mt-3">
                                            <label for="annualIncome" className="form-label">Pan Number</label>
                                            <input  type="text" className="form-control" id="annualIncome" />
                                        </div>
                                        <div className="col-12 col-md-4 mt-3">
                                            <label for="inputRelation" className="form-label">Policy Term</label>
                                            <select id="inputRelation" className="form-select">
                                                <option>1 Year</option>
                                                <option>2 Year</option>
                                                <option>3 Year</option>
                                            </select>
                                        </div>
                                        <div className="col-12 col-md-4 mt-3">
                                            <label for="discount" className="form-label">Discount</label>
                                            <input value="0" type="text" className="form-control" id="discount" disabled readOnly />
                                        </div>
                                        <div className="col-12 col-md-4 mt-3">
                                            <label for="premium" className="form-label">Premium</label>
                                            <input value="8091" type="text" className="form-control" id="premium" disabled readOnly />
                                        </div>
                                    </div>
                                </form>
                                </div>
                            </div>


                          
                            
        </div>
    )
} 
export default NomineeDetails