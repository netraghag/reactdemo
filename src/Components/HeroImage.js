import React from 'react'

function HeroImage() {
return (
<div>
    <div className="header_hero">
        <div className="container">
            <div className="row">
                <div className="col-lg-6 col-10 mx-auto">
                    <div className="header_hero__text">
                        <h2>
                            <span>music</span> for everyone
                        </h2>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s</p>

                        <div className="header_text__btn">
                            <a href="">download app now</a>
                            <a href="" className="btn__2">Start free trial</a>
                        </div>
                    </div>
                </div>
                <div className="col-lg-6 col-10 mx-auto">
                    <figure className="header_hero__img">
                        <img src="https://cdni.iconscout.com/illustration/premium/thumb/woman-listening-music-2839297-2361695.png"
                            alt="" />
                    </figure>
                </div>
            </div>
        </div>
    </div>
</div>

);
}
export default HeroImage