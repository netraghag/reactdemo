import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFemale, faMale } from '@fortawesome/free-solid-svg-icons'

function InsuredMember(){
    return(
        <div>
             <h4>Insured Member</h4>
                        <p>People being isured under the policy</p>
                        <ul className="nav nav-tabs" id="myTab" role="tablist">
                            <li className="nav-item" role="presentation">
                                <button className="nav-link active" id="you-tab" data-bs-toggle="tab" data-bs-target="#you" type="button" role="tab" aria-controls="you" aria-selected="true">You</button>
                            </li>
                            <li className="nav-item" role="presentation">
                                <button className="nav-link" id="spouse-tab" data-bs-toggle="tab" data-bs-target="#spouse" type="button" role="tab" aria-controls="spouse" aria-selected="false">Spouse</button>
                            </li>
                            <li className="nav-item" role="presentation">
                                <button className="nav-link" id="father-tab" data-bs-toggle="tab" data-bs-target="#father" type="button" role="tab" aria-controls="father" aria-selected="false">Father</button>
                            </li>
                        </ul>
                        <div className="tab-content mt-3" id="myTabContent">
                            <div className="tab-pane fade show active" id="you" role="tabpanel" aria-labelledby="you-tab">
                                <div className="card">
                                    <div className="card-header">
                                        You
                                    </div>
                                    <div className="card-body">
                                        <form >
                                            <div className="row">
                                                <div className="col-12 col-md-6">
                                                    <div>
                                                        <label for="nameField" className="form-label">Name</label>
                                                        <input type="text" className="form-control" id="nameField" placeholder="Enter name" />
                                                        <span className="small text-danger">
                                                            You must agree before submitting.
                                                        </span>
                                                    </div>
                                                </div>
                                                <div className="col-12 col-md-6 ">
                                                    <div className="d-flex">
                                                        <div className="w-100">
                                                            <label for="heightField" className="form-label">height</label>
                                                            <div className="d-flex">
                                                                <select id="inputHeight" className="form-select me-2">
                                                                    <option selected>FT</option>
                                                                    <option>1</option>
                                                                    <option>2</option>
                                                                    <option>3</option>
                                                                    <option>4</option>
                                                                    <option>5</option>
                                                                </select>
                                                                <select id="inputHeight2" className="form-select">
                                                                    <option selected>Inch</option>
                                                                    <option>1</option>
                                                                    <option>2</option>
                                                                    <option>3</option>
                                                                    <option>4</option>
                                                                    <option>5</option>
                                                                </select>
                                                            </div>
                                                            <span className="small text-danger">
                                                                Field Mandatory
                                                            </span>
                                                        </div>
                                                        <div className="ms-4 ">
                                                            <label for="weightield" className="form-label">Weight</label>
                                                            <input type="Name" className="form-control" id="weightield" />
                                                            <span className="small text-danger">
                                                                Field Mandatory
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div >
                                                <div className="col-12 col-md-6 mt-3">
                                                    <div>
                                                        <label for="nameField" className="form-label">Gender</label>
                                                        <div className="genderFields">
                                                            <div className="radio-toolbar">
                                                                <input type="radio" id="radioApple" name="radioFruit" value="apple" checked />
                                                                <label for="radioApple">
                                                                    <FontAwesomeIcon icon={faMale}></FontAwesomeIcon> Male</label>

                                                                <input type="radio" id="radioBanana" name="radioFruit" value="banana" />
                                                                <label for="radioBanana">
                                                                    <FontAwesomeIcon icon={faFemale}></FontAwesomeIcon> Female</label>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-12 col-md-6 mt-3">
                                                    <label for="inputDob" className="form-label">Date of Birth</label>
                                                    <input type="date" className="form-control" id="iinputDob" />
                                                </div>
                                                <div className="col-12 col-md-6 mt-3">
                                                    <label for="inputRelation" className="form-label">Relationship</label>
                                                    <select id="inputRelation" className="form-select">
                                                        <option selected>Choose...</option>
                                                        <option>Self</option>
                                                    </select>
                                                </div>
                                                <div className="col-12 col-md-6 mt-3">
                                                    <label for="inputProfession" className="form-label">Profession</label>
                                                    <select id="inputProfession" className="form-select">
                                                        <option selected>Choose...</option>
                                                        <option>Business</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div className="tab-pane fade" id="spouse" role="tabpanel" aria-labelledby="spouse-tab">
                                <div className="card">
                                    <div className="card-header">
                                        Spouse
                                    </div>
                                    <div className="card-body">
                                        <form >
                                            <div className="row">
                                                <div className="col-12 col-md-6">
                                                    <div>
                                                        <label for="nameField" className="form-label">Name</label>
                                                        <input type="text" className="form-control" id="nameField" placeholder="Enter name" />
                                                        <span className="small text-danger">
                                                            You must agree before submitting.
                                                        </span>
                                                    </div>
                                                </div>
                                                <div className="col-12 col-md-6 ">
                                                    <div className="d-flex">
                                                        <div className="w-100">
                                                            <label for="heightField" className="form-label">height</label>
                                                            <div className="d-flex">
                                                                <select id="inputHeight" className="form-select me-2">
                                                                    <option selected>FT</option>
                                                                    <option>1</option>
                                                                    <option>2</option>
                                                                    <option>3</option>
                                                                    <option>4</option>
                                                                    <option>5</option>
                                                                </select>
                                                                <select id="inputHeight2" className="form-select">
                                                                    <option selected>Inch</option>
                                                                    <option>1</option>
                                                                    <option>2</option>
                                                                    <option>3</option>
                                                                    <option>4</option>
                                                                    <option>5</option>
                                                                </select>
                                                            </div>
                                                            <span className="small text-danger">
                                                                Field Mandatory
                                                            </span>
                                                        </div>
                                                        <div className="ms-4 ">
                                                            <label for="weightield" className="form-label">Weight</label>
                                                            <input type="Name" className="form-control" id="weightield" />
                                                            <span className="small text-danger">
                                                                Field Mandatory
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div >
                                                <div className="col-12 col-md-6 mt-3">
                                                    <div>
                                                        <label for="nameField" className="form-label">Gender</label>
                                                        <div className="genderFields">
                                                            <div className="radio-toolbar">
                                                                <input type="radio" id="radioApple" name="radioFruit" value="apple" checked />
                                                                <label for="radioApple">
                                                                    <FontAwesomeIcon icon={faMale}></FontAwesomeIcon> Male</label>

                                                                <input type="radio" id="radioBanana" name="radioFruit" value="banana" />
                                                                <label for="radioBanana">
                                                                    <FontAwesomeIcon icon={faFemale}></FontAwesomeIcon> Female</label>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-12 col-md-6 mt-3">
                                                    <label for="inputDob" className="form-label">Date of Birth</label>
                                                    <input type="date" className="form-control" id="iinputDob" />
                                                </div>
                                                <div className="col-12 col-md-6 mt-3">
                                                    <label for="inputRelation" className="form-label">Relationship</label>
                                                    <select id="inputRelation" className="form-select">
                                                        <option selected>Choose...</option>
                                                        <option>Self</option>
                                                    </select>
                                                </div>
                                                <div className="col-12 col-md-6 mt-3">
                                                    <label for="inputProfession" className="form-label">Profession</label>
                                                    <select id="inputProfession" className="form-select">
                                                        <option selected>Choose...</option>
                                                        <option>Business</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div className="tab-pane fade" id="father" role="tabpanel" aria-labelledby="father-tab">Father</div>

                        </div>
        </div>
    )
}
export default InsuredMember