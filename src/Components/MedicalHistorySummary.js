import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFemale, faMale } from '@fortawesome/free-solid-svg-icons'
function MedicalHistorySummary() {
    return (
        <div>
            <ul className="nav nav-tabs" id="myTab" role="tablist">
                <li className="nav-item" role="presentation">
                    <button className="nav-link active" id="you-tab2" data-bs-toggle="tab" data-bs-target="#you2" type="button" role="tab" aria-controls="you" aria-selected="true">You</button>
                </li>
                <li className="nav-item" role="presentation">
                    <button className="nav-link" id="spouse-tab2" data-bs-toggle="tab" data-bs-target="#spouse2" type="button" role="tab" aria-controls="spouse" aria-selected="false">Spouse</button>
                </li>
                <li className="nav-item" role="presentation">
                    <button className="nav-link" id="father-tab2" data-bs-toggle="tab" data-bs-target="#father2" type="button" role="tab" aria-controls="father" aria-selected="false">Father</button>
                </li>
            </ul>
            <div className="tab-content mt-3" id="myTabContent">
                <div className="tab-pane fade show active" id="you2" role="tabpanel" aria-labelledby="you-tab2">
                    <div className="">

                        <div className="">
                            <form >
                                <div className="row mt-3">
                                    <div className="col-12 col-md-8">
                                        1.Do you have any health problems? Has the person proposed for insurance been advised for any medical treatment ? If yes please mention details.
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="inlineRadioOptions3" id="yes" value="yes" />
                                            <label className="form-check-label" for="yes">Yes</label>
                                        </div>
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="inlineRadioOptions3" id="no" value="no" checked disabled />
                                            <label className="form-check-label" for="no">No</label>
                                        </div>
                                    </div>
                                    <div className="form-floating mt-2">
                                        <textarea className="form-control" placeholder="Leave a comment here" id="floatingTextarea"></textarea>
                                        <label for="floatingTextarea" className="ms-3">Reason</label>
                                    </div>
                                </div>
                                <div className="row mt-3">
                                    <div className="col-12 col-md-8">
                                        2.Do you have any health problems? Has the person proposed for insurance been advised for any medical treatment ? If yes please mention details.
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="inlineRadioOptions1" id="yes" value="yes" />
                                            <label className="form-check-label" for="yes">Yes</label>
                                        </div>
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="inlineRadioOptions1" id="no" value="no" checked disabled />
                                            <label className="form-check-label" for="no">No</label>
                                        </div>
                                    </div>
                                    <div className="form-floating mt-2">
                                        <textarea className="form-control" placeholder="Leave a comment here" id="floatingTextarea"></textarea>
                                        <label for="floatingTextarea" className="ms-3">Reason</label>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div className="tab-pane fade" id="spouse2" role="tabpanel" aria-labelledby="spouse-tab2">
                    <div className="">
                        <div className="">
                            <form >
                                <div className="row mt-3">
                                    <div className="col-12 col-md-8">
                                        1.Do you have any health problems? Has the person proposed for insurance been advised for any medical treatment ? If yes please mention details.
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="inlineRadioOptions2" id="yes" value="yes" />
                                            <label className="form-check-label" for="yes">Yes</label>
                                        </div>
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="inlineRadioOptions2" id="no" value="no" checked disabled />
                                            <label className="form-check-label" for="no">No</label>
                                        </div>
                                    </div>
                                    <div className="form-floating mt-2">
                                        <textarea className="form-control" placeholder="Leave a comment here" id="floatingTextarea"></textarea>
                                        <label for="floatingTextarea" className="ms-3">Reason</label>
                                    </div>
                                </div>
                                <div className="row mt-3">
                                    <div className="col-12 col-md-8">
                                        2.Do you have any health problems? Has the person proposed for insurance been advised for any medical treatment ? If yes please mention details.
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="inlineRadioOptions" id="yes" value="yes" />
                                            <label className="form-check-label" for="yes">Yes</label>
                                        </div>
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="inlineRadioOptions" id="no" value="no" checked disabled />
                                            <label className="form-check-label" for="no">No</label>
                                        </div>
                                    </div>
                                    <div className="form-floating mt-2">
                                        <textarea className="form-control" placeholder="Leave a comment here" id="floatingTextarea"></textarea>
                                        <label for="floatingTextarea" className="ms-3">Reason</label>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div className="tab-pane fade" id="father2" role="tabpanel" aria-labelledby="father-tab2">
                    <div className="">
                        <div className="">
                            <form >
                                <div className="row mt-3">
                                    <div className="col-12 col-md-8">
                                        1.Do you have any health problems? Has the person proposed for insurance been advised for any medical treatment ? If yes please mention details.
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="inlineRadioOptions2" id="yes" value="yes" />
                                            <label className="form-check-label" for="yes">Yes</label>
                                        </div>
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="inlineRadioOptions2" id="no" value="no" checked disabled />
                                            <label className="form-check-label" for="no">No</label>
                                        </div>
                                    </div>
                                    <div className="form-floating mt-2">
                                        <textarea className="form-control" placeholder="Leave a comment here" id="floatingTextarea"></textarea>
                                        <label for="floatingTextarea" className="ms-3">Reason</label>
                                    </div>
                                </div>
                                <div className="row mt-3">
                                    <div className="col-12 col-md-8">
                                        2.Do you have any health problems? Has the person proposed for insurance been advised for any medical treatment ? If yes please mention details.
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="inlineRadioOptions" id="yes" value="yes" />
                                            <label className="form-check-label" for="yes">Yes</label>
                                        </div>
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="inlineRadioOptions" id="no" value="no" checked disabled />
                                            <label className="form-check-label" for="no">No</label>
                                        </div>
                                    </div>
                                    <div className="form-floating mt-2">
                                        <textarea className="form-control" placeholder="Leave a comment here" id="floatingTextarea"></textarea>
                                        <label for="floatingTextarea" className="ms-3">Reason</label>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    )
}
export default MedicalHistorySummary