import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFemale, faMale } from '@fortawesome/free-solid-svg-icons'
function MedicalHistory() {
    return (
        <div>
            <h4>Medical History</h4>
            <p>People being isured under the policy</p>
            <ul className="nav nav-tabs" id="myTab" role="tablist">
                <li className="nav-item" role="presentation">
                    <button className="nav-link active" id="you-tab" data-bs-toggle="tab" data-bs-target="#you" type="button" role="tab" aria-controls="you" aria-selected="true">You</button>
                </li>
                <li className="nav-item" role="presentation">
                    <button className="nav-link" id="spouse-tab" data-bs-toggle="tab" data-bs-target="#spouse" type="button" role="tab" aria-controls="spouse" aria-selected="false">Spouse</button>
                </li>
                <li className="nav-item" role="presentation">
                    <button className="nav-link" id="father-tab" data-bs-toggle="tab" data-bs-target="#father" type="button" role="tab" aria-controls="father" aria-selected="false">Father</button>
                </li>
            </ul>
            <div className="tab-content mt-3" id="myTabContent">
                <div className="tab-pane show active" id="you" role="tabpanel" aria-labelledby="you-tab">
                    <div className="card">
                        <div className="card-header d-flex">
                            <span>You</span>

                            <div className="form-check ms-auto">
                                <input className="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                <label className="form-check-label" for="flexCheckDefault">
                                    Same For All
                                </label>
                            </div>
                        </div>
                        <div className="card-body">
                            <form >
                                <div className="row mt-3">
                                    <div className="col-12 col-md-8">
                                        1.Do you have any health problems? Has the person proposed for insurance been advised for any medical treatment ? If yes please mention details.
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="inlineRadioOptions" id="yes" value="yes" />
                                            <label className="form-check-label" for="yes">Yes</label>
                                        </div>
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="inlineRadioOptions" id="no" value="no" />
                                            <label className="form-check-label" for="no">No</label>
                                        </div>
                                    </div>
                                    <div className="form-floating mt-2">
                                        <textarea className="form-control" placeholder="Leave a comment here" id="floatingTextarea"></textarea>
                                        <label for="floatingTextarea" className="ms-3">Reason</label>
                                    </div>
                                </div>
                                <div className="row mt-3">
                                    <div className="col-12 col-md-8">
                                        2.Do you have any health problems? Has the person proposed for insurance been advised for any medical treatment ? If yes please mention details.
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="inlineRadioOptions" id="yes" value="yes" />
                                            <label className="form-check-label" for="yes">Yes</label>
                                        </div>
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="inlineRadioOptions" id="no" value="no" />
                                            <label className="form-check-label" for="no">No</label>
                                        </div>
                                    </div>
                                    <div className="form-floating mt-2">
                                        <textarea className="form-control" placeholder="Leave a comment here" id="floatingTextarea"></textarea>
                                        <label for="floatingTextarea" className="ms-3">Reason</label>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div className="tab-pane" id="spouse" role="tabpanel" aria-labelledby="spouse-tab">
                    <div className="card">
                        <div className="card-header">
                            Spouse
                        </div>
                        <div className="card-body">
                            <form >
                                <div className="row mt-3">
                                    <div className="col-12 col-md-8">
                                        1.Do you have any health problems? Has the person proposed for insurance been advised for any medical treatment ? If yes please mention details.
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="inlineRadioOptions" id="yes" value="yes" />
                                            <label className="form-check-label" for="yes">Yes</label>
                                        </div>
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="inlineRadioOptions" id="no" value="no" />
                                            <label className="form-check-label" for="no">No</label>
                                        </div>
                                    </div>
                                    <div className="form-floating mt-2">
                                        <textarea className="form-control" placeholder="Leave a comment here" id="floatingTextarea"></textarea>
                                        <label for="floatingTextarea" className="ms-3">Reason</label>
                                    </div>
                                </div>
                                <div className="row mt-3">
                                    <div className="col-12 col-md-8">
                                        2.Do you have any health problems? Has the person proposed for insurance been advised for any medical treatment ? If yes please mention details.
                                    </div>
                                    <div className="col-12 col-md-4">
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="inlineRadioOptions" id="yes" value="yes" />
                                            <label className="form-check-label" for="yes">Yes</label>
                                        </div>
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="inlineRadioOptions" id="no" value="no" />
                                            <label className="form-check-label" for="no">No</label>
                                        </div>
                                    </div>
                                    <div className="form-floating mt-2">
                                        <textarea className="form-control" placeholder="Leave a comment here" id="floatingTextarea"></textarea>
                                        <label for="floatingTextarea" className="ms-3">Reason</label>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div className="tab-pane " id="father" role="tabpanel" aria-labelledby="father-tab">
                    <div className="card">
                        <div className="card-header">
                            Father
                        </div>
                        <div className="card-body">
                            <form >
                                <div className="row mt-3">
                                    <table className="table table-striped table-sm">
                                        <thead className="text-center">
                                            <th>Medical Questions</th>
                                            <th >Yes</th>
                                            <th>No</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td> 1.Do you have any health problems? Has the person proposed for insurance been advised for any medical treatment ? If yes please mention details.</td>
                                                <td>
                                                    <div className="dropdown">
                                                        <button className="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                            Selected (2)
                                                        </button>
                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                            <li><a className="dropdown-item">
                                                                <div className="form-check">
                                                                    <input className="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                                    <label className="form-check-label" for="flexCheckDefault">
                                                                        You
                                                                    </label>
                                                                </div>
                                                            </a></li>
                                                            <li><a className="dropdown-item">
                                                                <div className="form-check">
                                                                    <input className="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                                    <label className="form-check-label" for="flexCheckDefault">
                                                                        Spouse
                                                                    </label>
                                                                </div></a></li>
                                                            <li><a className="dropdown-item">
                                                                <div className="form-check">
                                                                    <input className="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                                    <label className="form-check-label" for="flexCheckDefault">
                                                                        Father
                                                                    </label>
                                                                </div>
                                                            </a></li>
                                                            <li><a className="dropdown-item">
                                                                <div className="form-check">
                                                                    <input className="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                                    <label className="form-check-label" for="flexCheckDefault">
                                                                        Mother
                                                                    </label>
                                                                </div>
                                                            </a></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div className="dropdown">
                                                        <button className="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                            Selected (2)
                                                        </button>
                                                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                            <li><a className="dropdown-item">
                                                                <div className="form-check">
                                                                    <input className="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                                    <label className="form-check-label" for="flexCheckDefault">
                                                                        You
                                                                    </label>
                                                                </div>
                                                            </a></li>
                                                            <li><a className="dropdown-item">
                                                                <div className="form-check">
                                                                    <input className="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                                    <label className="form-check-label" for="flexCheckDefault">
                                                                        Spouse
                                                                    </label>
                                                                </div></a></li>
                                                            <li><a className="dropdown-item">
                                                                <div className="form-check">
                                                                    <input className="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                                    <label className="form-check-label" for="flexCheckDefault">
                                                                        Father
                                                                    </label>
                                                                </div>
                                                            </a></li>
                                                            <li><a className="dropdown-item">
                                                                <div className="form-check">
                                                                    <input className="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                                    <label className="form-check-label" for="flexCheckDefault">
                                                                        Mother
                                                                    </label>
                                                                </div>
                                                            </a></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div className="form-floating mt-2">
                                    <textarea className="form-control" placeholder="Leave a comment here" id="floatingTextarea"></textarea>
                                    <label for="floatingTextarea" className="ms-3">Reason (You)</label>
                                </div>
                                <div className="form-floating mt-2">
                                    <textarea className="form-control" placeholder="Leave a comment here" id="floatingTextarea"></textarea>
                                    <label for="floatingTextarea" className="ms-3">Reason (Spouse)</label>
                                </div>
                                <div className="form-floating mt-2">
                                    <textarea className="form-control" placeholder="Leave a comment here" id="floatingTextarea"></textarea>
                                    <label for="floatingTextarea" className="ms-3">Reason (Father)</label>
                                </div>
                                <div className="form-floating mt-2">
                                    <textarea className="form-control" placeholder="Leave a comment here" id="floatingTextarea"></textarea>
                                    <label for="floatingTextarea" className="ms-3">Reason (Mother)</label>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    )
}
export default MedicalHistory