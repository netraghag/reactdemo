import React from 'react'

function Section() {
    return (
        <div>
            <div>
                <div className="container">
                    <div className="row">
                        <div className="col-6">
                            <figure className="header_hero__img">
                                <img src="https://cdni.iconscout.com/illustration/premium/thumb/boy-listening-music-2527775-2114678.png" alt="" />
                            </figure>
                        </div>
                        <div className="col-lg-6 col-10 mx-auto">
                            <div className="header_hero__text_2">
                                <h2>
                                    Importance of <span>music:</span>
                                </h2>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                                    been the industry's standard dummy text ever since the 1500s</p>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="bg-light">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-6 col-10 mx-auto">
                            <div className="header_hero__text_2">
                                <h2>
                                Magical Powers of <span>music:</span>
                                </h2>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                                    been the industry's standard dummy text ever since the 1500s</p>
                            </div>
                        </div>
                        
                        <div className="col-6">
                            <figure className="header_hero__img">
                                <img src="https://cdni.iconscout.com/illustration/premium/thumb/african-american-man-listening-music-from-cloud-server-2948548-2447315.png" alt="" />
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Section