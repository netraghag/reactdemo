import React from 'react'
import Shimmer from "react-shimmer-effect";

function MotorSummary() {
    return (
        <div>
            <div className="d-flex my-2">
                <h4 className="my-auto">Summary Details</h4> <span className="ms-auto my-auto btn-sm btn-primary rounded-pill">Edit Proposal</span>
            </div>
            <div className="accordion " id="accordionFlushExample">
                <div className="accordion-item">
                    <h2 className="accordion-header" id="flush-headingOne">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                            <b> BIKE OWNER DETAILS </b>

                        </button>
                    </h2>
                    <div id="flush-collapseOne" className="accordion-collapse collapse show" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                        <div className="accordion-body">
                            <div className="row">
                                <div className="col-12 col-md-6">
                                    <div className="mb-3">
                                        <label for="ProposalName" className="col-form-label pb-0">Full Name</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Netra Anant Ghag
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="mb-3">
                                        <label for="dateofbirth" className="col-form-label pb-0">Date of Birth</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                31/10/1991
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="mb-3">
                                        <label for="mobileNumber" className="col-form-label pb-0">Mobile No.</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                9833587788
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="mb-3">
                                        <label for="emailId" className="col-form-label pb-0">Email Id</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                netraghag@gmail.com
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="accordion-item">
                    <h2 className="accordion-header" id="flush-headingTwo">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                            <b>ADDRESS DETAILS</b>
                        </button>
                    </h2>
                    <div id="flush-collapseTwo" className="accordion-collapse collapse  show" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                        <div className="accordion-body">
                            <div className="row">
                                <div className="col-12 col-md-6">
                                    <div className="mb-3">
                                        <label for="addone" className="col-form-label pb-0">Address Line 1</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                G1A,Akruti Corporate Park, Lal Bahadur Shastri Road, Ambedka
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="mb-3">
                                        <label for="addtwo" className="col-form-label pb-0">Address Line 2</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                G1A,Akruti Corporate Park, Lal Bahadur Shastri Road, Ambedka
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="mb-3">
                                        <label for="pincode" className="col-form-label pb-0">Pincode</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                400078
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="mb-3">
                                        <label for="city" className="col-form-label pb-0">City</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Mumbai
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="mb-3">
                                        <label for="city" className="col-form-label pb-0">State</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Maharashtra
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 ">
                                    <div className="form-check">
                                        <input className="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked disabled />
                                        <label className="form-check-label" for="flexCheckChecked">
                                            Communication Address is same as Vehicle Registration Address
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="accordion-item">
                    <h2 className="accordion-header" id="flush-headingThree">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                            <b>VEHICLE DETAILS</b>
                        </button>
                    </h2>
                    <div id="flush-collapseThree" className="accordion-collapse collapse show" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                        <div className="accordion-body">
                            <div className="row">
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="makeModal" className="col-form-label pb-0">Make & Model</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                HONDA ACTIVA
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="variant" className="col-form-label pb-0">Variant</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                125 (124 CC)
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="fuelType" className="col-form-label pb-0">Fuel Type</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Petrol
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="RTOLocation" className="col-form-label pb-0">RTO Location</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                MH09-KOLHAPUR
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="regNumber" className="col-form-label pb-0">Vehicle Registration No.</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                MH-09-as-4512
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Engine No.</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                5a4sdd64a6s5d
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Chassis No.</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                54AS56D4A65
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">When was your bike manufactured?</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                01/2018
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Vehicle Registration Date</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                01/01/2018
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Previous Insurer</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Liberty Videocon Insurance
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Previous Policy No.</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                a654sd654as65d4
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Claim Made Last Year?</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                NO
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Policy Start Date</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                05/11/2020
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Policy Expiry Date</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                04/11/2021
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Policy Type</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Comprehensive / Package
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="accordion-item">
                    <h2 className="accordion-header" id="flush-headingFour">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
                            <b>NOMINEE DETAILS</b>
                        </button>
                    </h2>
                    <div id="flush-collapseFour" className="accordion-collapse collapse show" aria-labelledby="flush-headingFour" data-bs-parent="#accordionFlushExample">
                        <div className="accordion-body">
                            <div className="row">
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Nominee Full Name</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Netra Ghag
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Nominee Age</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                33
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Relationship with Nominee</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Father
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="accordion-item">
                    <h2 className="accordion-header" id="flush-headingFive">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false" aria-controls="flush-collapseFive">
                            <b>MEMBERS DETAILS</b>
                        </button>
                    </h2>
                    <div id="flush-collapseFive" className="accordion-collapse collapse show" aria-labelledby="flush-headingFive" data-bs-parent="#accordionFlushExample">
                        <div className="accordion-body">
                            <div className="row">
                                <div className="bg-secondary text-white mb-3">
                                    <h5 className="my-auto">You</h5>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Full Name</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Anant Ghag
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Height</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                5'8
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Weight</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                65
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Gender</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Male
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Date of Birth</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                02/06/1962
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Relationship</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Father
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Profession</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Buisness
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="bg-secondary text-white mb-3">
                                    <h5 className="my-auto">Spouse</h5>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Full Name</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Anant Ghag
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Height</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                5'8
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Weight</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                65
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Gender</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Male
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Date of Birth</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                02/06/1962
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Relationship</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Father
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Profession</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Buisness
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="bg-secondary text-white mb-3">
                                    <h5 className="my-auto">Father</h5>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Full Name</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Anant Ghag
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Height</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                5'8
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Weight</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                65
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Gender</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Male
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Date of Birth</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                02/06/1962
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Relationship</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Father
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Profession</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Buisness
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    )
}
export default MotorSummary