import React from "react";

const Contact = props => {
    return(
        <>
    <h1>Contact {props.name}</h1>
    {props.children}
    </>
    )};

export default Contact; 