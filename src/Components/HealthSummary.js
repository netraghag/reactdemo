import React from "react";
import InsuredMember from "./InsuredMember";
import InsuredMemberSummary from "./InsuredMemberSummary";
import MedicalHistory from "./MedicalHistory";
import MedicalHistorySummary from "./MedicalHistorySummary";
import { Link } from "react-router-dom";
function HealthSummary() {
    return (
        <div>
            <div className="d-flex my-2">
                <h4 className="my-auto">Summary Details</h4> <Link to="proposal-form" className="ms-auto my-auto btn-sm btn-primary rounded-pill">Edit Proposal</Link>
            </div>
            <div className="accordion " id="accordionFlushExample">
                <div className="accordion-item">
                    <h2 className="accordion-header" id="flush-headingOne">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                            <b> Proposer Details </b>

                        </button>
                    </h2>
                    <div id="flush-collapseOne" className="accordion-collapse collapse show" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                        <div className="accordion-body">
                            <div className="row">
                                <div className="col-12 col-md-8">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Full Name</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Netra Anant Ghag
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Marital Status</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Single
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Gender</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Female
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Date of Birth</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                31/10/1991
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Relationship</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                31/10/1991
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Nationality</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Indian
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Anual Income</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                5,00,000/-
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Pan Number</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                ABC154782D
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Policy Term</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                1 Year
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Discount</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                0
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Premium</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                8091
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="accordion-item">
                    <h2 className="accordion-header" id="flush-headingTwo">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                            <b>Members Detail</b>
                        </button>
                    </h2>
                    <div id="flush-collapseTwo" className="accordion-collapse collapse  show" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                        <div className="accordion-body">
                           <InsuredMemberSummary/>
                        </div>
                    </div>
                </div>
                <div className="accordion-item">
                    <h2 className="accordion-header" id="flush-headingThree">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                            <b>Medical History</b>
                        </button>
                    </h2>
                    <div id="flush-collapseThree" className="accordion-collapse collapse show" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                        <div className="accordion-body">
                            <MedicalHistorySummary/>
                        </div>
                    </div>
                </div>
                <div className="accordion-item">
                    <h2 className="accordion-header" id="flush-headingFour">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
                            <b>NOMINEE DETAILS</b>
                        </button>
                    </h2>
                    <div id="flush-collapseFour" className="accordion-collapse collapse show" aria-labelledby="flush-headingFour" data-bs-parent="#accordionFlushExample">
                        <div className="accordion-body">
                            <div className="row">
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Nominee Full Name</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Netra Ghag
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Nominee Age</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                33
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-4">
                                    <div className="mb-3">
                                        <label for="" className="col-form-label pb-0">Relationship with Nominee</label>
                                        <div className="">
                                            <span className="form-control-plaintext text-secondary pt-0">
                                                Father
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    )
}
export default HealthSummary