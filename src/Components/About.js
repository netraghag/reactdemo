import React from "react";
import Welcome from "./ClassComponent";
import Contact from "./Contact";

const About = () => {
    return (
        <>
            <h1>About1</h1>
            <Contact name="netra">
                <p>This children props</p>
            </Contact>
            <Contact name="Kishori">
                <button>Hello</button>
            </Contact>
            <Welcome name="Trupti">
                <u>This is children props in class component </u>
            </Welcome>
        </>
    )
};

export default About;